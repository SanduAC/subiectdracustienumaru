package com.example.alexandrusandu.kotlinlearn

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    val add: (Int) -> (Int) -> Int = { a -> { b -> a + b } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val rez: Int = add(3)(4)
//        Log.d("RESULT sCUCUCUCUCCU", rez.toString())
        Toast.makeText(applicationContext, add(3)(4).toString(), Toast.LENGTH_LONG).show()

        val tmp = listOf("Kotlin", "Java", "Scala")

        val tmp1 = List(3) { index -> 3 }
        val res = tmp.mapIndexed { index: Int, c: String ->
            index to c
        }

        Log.d("TEST TEST", res.toString())

    }





}
