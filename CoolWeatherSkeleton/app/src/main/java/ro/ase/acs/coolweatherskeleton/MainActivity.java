package ro.ase.acs.coolweatherskeleton;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import ro.ase.acs.coolweatherskeleton.Models.LocationModel;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_ADD_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setControls();
    }

    private void setControls(){
        findViewById(R.id.viewOnlineButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeActivityToExternalUrl();
            }
        });
    }

    public void changeActivityToAddLocationActivity(View view){
        startActivityForResult(new Intent(MainActivity.this, AddLocationActivity.class), REQUEST_CODE_ADD_LOCATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_ADD_LOCATION) {
            if (resultCode == RESULT_OK) {
                LocationModel locationModel = (LocationModel) data.getSerializableExtra("RESULT");
                Toast.makeText(MainActivity.this,locationModel.getCity(), Toast.LENGTH_LONG).show();

            }
        }
    }

    private void changeActivityToExternalUrl() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vremea.ro/bucuresti/bucuresti/")));
    }
}
