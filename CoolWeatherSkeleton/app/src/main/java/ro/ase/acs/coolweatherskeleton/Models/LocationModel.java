package ro.ase.acs.coolweatherskeleton.Models;

import java.io.Serializable;

public class LocationModel implements Serializable {
    private String city;
    private String country;

    public LocationModel(String city, String country) {
        this.city = city;
        this.country = country;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(city);
        sb.append(", ");
        sb.append(country);
        return sb.toString();
    }

    public String getCity(){
        return city;
    }
}
