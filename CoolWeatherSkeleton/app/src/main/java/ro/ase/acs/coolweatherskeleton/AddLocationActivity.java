package ro.ase.acs.coolweatherskeleton;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ro.ase.acs.coolweatherskeleton.Models.LocationModel;

public class AddLocationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        setControls();
    }

    private void setControls() {
        findViewById(R.id.addLocationButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((EditText)findViewById(R.id.et_city)).getText().toString().equals("")){
                    Toast.makeText(AddLocationActivity.this, "Please enter the city!", Toast.LENGTH_LONG).show();
                    return;
                }
                if(((EditText)findViewById(R.id.et_country)).getText().toString().equals("")){
                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddLocationActivity.this);
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setMessage("Please enter the country!");
                    dialogBuilder.setPositiveButton("Ok", null);
                    dialogBuilder.show();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("RESULT", new LocationModel(((EditText)findViewById(R.id.et_city)).getText().toString(), ((EditText)findViewById(R.id.et_country)).getText().toString()));
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
