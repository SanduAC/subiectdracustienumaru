package examen.androidno3rdparty.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import examen.androidno3rdparty.models.DataPackage;

public class Client {
    public static final String BASE_URL = "http://www.mocky.io/%s";

    public interface GetPackagesFromServerCallBack{
        void success(List<DataPackage> dataPackages);
        void fail(String errorMessage);
    }

    public static void getPackagesFromServer(GetPackagesFromServerCallBack getPackagesFromServerCallBack) {
        HttpURLConnection connection = null;
        try {
            URL url=new URL(getURL("v2/5c4dd2e33100001104c41b9d/"));
            connection = (HttpURLConnection)url.openConnection();
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader=new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder=new StringBuilder();
            String line;
            while((line = reader.readLine())!=null){
                stringBuilder.append(line);
            }
            String response=stringBuilder.toString();
            //TODO PARSE JSON, BORED TO DO IT

        } catch (MalformedURLException e) {
            e.printStackTrace();
            getPackagesFromServerCallBack.fail(e.getLocalizedMessage());
        } catch (IOException e) {
            e.printStackTrace();
            getPackagesFromServerCallBack.fail(e.getLocalizedMessage());
        }
    }


    public static String getURL(String post) {
        return String.format(BASE_URL, post);
    }
}
