package examen.androidno3rdparty.ui.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import examen.androidno3rdparty.MainActivity;
import examen.androidno3rdparty.R;
import examen.androidno3rdparty.models.DataPackage;
import examen.androidno3rdparty.models.DataPackagesForIntent;
import examen.androidno3rdparty.tools.Constants;
import examen.androidno3rdparty.ui.ChartActivity;
import examen.androidno3rdparty.ui.CreatePackageActivity;
import examen.androidno3rdparty.ui.ListActivity;

public class ActionBarActivity extends AppCompatActivity {
    protected List<DataPackage> packagesList=new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitleText();
        getList();
    }

    private void getList() {
        if(getIntent().getSerializableExtra(Constants.PACKAGES_LIST_ARGUMENT)!=null){
            packagesList.addAll(((DataPackagesForIntent)getIntent().getSerializableExtra(Constants.PACKAGES_LIST_ARGUMENT)).dataPackages);
        }
    }

    private void setTitleText() {
        if(ActionBarActivity.this instanceof MainActivity){
            ((TextView)findViewById(R.id.title)).setText(getString(R.string.menu));
        }else if(ActionBarActivity.this instanceof CreatePackageActivity){
            ((TextView)findViewById(R.id.title)).setText(getString(R.string.send_package_title));
        }else if(ActionBarActivity.this instanceof ListActivity){
            ((TextView)findViewById(R.id.title)).setText(getString(R.string.list));
        }else if(ActionBarActivity.this instanceof ChartActivity){
            ((TextView)findViewById(R.id.title)).setText(getString(R.string.chart));
        }
    }

    public void changeActivityWithAnimantionAndPackagesExtra(Class<?> activityTo){
        Intent intent=new Intent(ActionBarActivity.this, activityTo);
        intent.putExtra(Constants.PACKAGES_LIST_ARGUMENT, new DataPackagesForIntent(packagesList));
        startActivity(intent);
        overridePendingTransition(R.anim.in_animation, R.anim.no_animation);
    }
    public void shortText(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
