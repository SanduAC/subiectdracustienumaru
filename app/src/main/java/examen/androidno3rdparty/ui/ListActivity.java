package examen.androidno3rdparty.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import examen.androidno3rdparty.R;
import examen.androidno3rdparty.adapter.PackageListAdapter;
import examen.androidno3rdparty.models.DataPackage;
import examen.androidno3rdparty.models.DataPackagesForIntent;
import examen.androidno3rdparty.tools.Constants;
import examen.androidno3rdparty.ui.common.ActionBarActivity;

public class ListActivity extends ActionBarActivity {
    private ListView lvPackages;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_list);
        super.onCreate(savedInstanceState);
        getViewReferences();
        setListView(0);
        setControls();
    }

    private void setControls() {
        ((Spinner)findViewById(R.id.sp_filter)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setListView(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getViewReferences() {
        lvPackages=findViewById(R.id.lv_package);
    }

    private void setListView(int position) {
        List<DataPackage> packagesToShow=new ArrayList<>();
        if(position==0){
            packagesToShow.addAll(packagesList);
        }else if(position==1){
            for(DataPackage item:packagesList){
                if(item.packageType.equals(DataPackage.PackageType.position)){
                    packagesToShow.add(item);
                }
            }
        }else{
            for(DataPackage item:packagesList){
                if(item.packageType.equals(DataPackage.PackageType.state)){
                    packagesToShow.add(item);
                }
            }
        }

        lvPackages.setAdapter(new PackageListAdapter(packagesToShow, ListActivity.this));
    }

    public void onItemClicked(DataPackage item) {
        Intent intent=new Intent(ListActivity.this, CreatePackageActivity.class);
        intent.putExtra(Constants.PACKAGES_LIST_ARGUMENT, new DataPackagesForIntent(packagesList));
        intent.putExtra(Constants.EDIT_OR_CREATE_ARGUMENT, true);
        intent.putExtra(Constants.PACKAGE_ARGUMENT, item);
        startActivity(intent);
        overridePendingTransition(R.anim.in_animation, R.anim.no_animation);
        finish();

    }
}
