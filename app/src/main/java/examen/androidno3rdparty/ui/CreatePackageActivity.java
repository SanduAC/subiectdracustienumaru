package examen.androidno3rdparty.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import examen.androidno3rdparty.MainActivity;
import examen.androidno3rdparty.R;
import examen.androidno3rdparty.models.DataPackage;
import examen.androidno3rdparty.models.DataPackagesForIntent;
import examen.androidno3rdparty.tools.Constants;
import examen.androidno3rdparty.ui.common.ActionBarActivity;

public class CreatePackageActivity extends ActionBarActivity {
    private boolean edit = false;
    private DataPackage dataPackage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_package);
        super.onCreate(savedInstanceState);
        getScope();
        if(edit){
            setViewsForEdit();
        }
        setControls();
    }

    private void setControls() {
        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((Spinner)findViewById(R.id.sp_package_type)).getSelectedItemPosition()==0){
                    shortText("Pleaase select the package type!");
                    return;
                }
                if(((EditText)findViewById(R.id.et_longitude)).getText().toString().equals("")){
                    shortText("Pleaase enter the longitude!");
                    return;

                }
                if(((EditText)findViewById(R.id.et_latitude)).getText().toString().equals("")){
                    shortText("Pleaase enter the latitude!");
                    return;
                }
                if(((Spinner)findViewById(R.id.sp_package_type)).getSelectedItemPosition()==1){
                    dataPackage.packageType=DataPackage.PackageType.position;
                }else{
                    dataPackage.packageType=DataPackage.PackageType.state;
                }
                dataPackage.latitude=Double.valueOf(((EditText)findViewById(R.id.et_latitude)).getText().toString());
                dataPackage.longitude=Double.valueOf(((EditText)findViewById(R.id.et_longitude)).getText().toString());

                if(edit){
                    for(DataPackage item:packagesList){
                        if(item.packageId==dataPackage.packageId){
                            item.edit(dataPackage);
                        }
                    }
                }else{
                    packagesList.add(dataPackage);
                }

                Intent intent = new Intent(CreatePackageActivity.this, MainActivity.class);
                intent.putExtra(Constants.PACKAGES_LIST_ARGUMENT, new DataPackagesForIntent(packagesList));
                startActivity(intent);
                overridePendingTransition(R.anim.no_animation, R.anim.out_animation);
                finish();
            }
        });
    }

    private void setViewsForEdit() {
        ((EditText)findViewById(R.id.et_latitude)).setText(String.valueOf(dataPackage.latitude));
        ((EditText)findViewById(R.id.et_longitude)).setText(String.valueOf(dataPackage.longitude));

        if(DataPackage.PackageType.position.equals(dataPackage.packageType)){
            ((Spinner)findViewById(R.id.sp_package_type)).setSelection(1);
        }else{
            ((Spinner)findViewById(R.id.sp_package_type)).setSelection(2);
        }
    }

    private void getScope() {
        if (getIntent().getBooleanExtra(Constants.EDIT_OR_CREATE_ARGUMENT, false)) {
            edit = getIntent().getBooleanExtra(Constants.EDIT_OR_CREATE_ARGUMENT, false);
            dataPackage = (DataPackage) getIntent().getSerializableExtra(Constants.PACKAGE_ARGUMENT);
        }else{
            dataPackage=new DataPackage();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreatePackageActivity.this, MainActivity.class);
        intent.putExtra(Constants.PACKAGES_LIST_ARGUMENT, new DataPackagesForIntent(packagesList));
        startActivity(intent);
        overridePendingTransition(R.anim.no_animation, R.anim.out_animation);
        finish();
    }
}
