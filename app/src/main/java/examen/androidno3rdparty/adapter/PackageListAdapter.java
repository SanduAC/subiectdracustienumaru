package examen.androidno3rdparty.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import examen.androidno3rdparty.R;
import examen.androidno3rdparty.models.DataPackage;
import examen.androidno3rdparty.ui.ListActivity;

public class PackageListAdapter extends ArrayAdapter<DataPackage> {

    private List<DataPackage> items;
    private ListActivity listActivity;

    public PackageListAdapter(List<DataPackage> items, ListActivity listActivity) {
        super(listActivity, R.layout.row_package, items);
        this.items = items;
        this.listActivity = listActivity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final DataPackage item = items.get(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(listActivity);
            convertView = inflater.inflate(R.layout.row_package, null);
        }

        TextView tv = convertView.findViewById(R.id.package_id);
        tv.setText(String.valueOf(item.packageId));

        TextView latitude = convertView.findViewById(R.id.something);
        latitude.setText(String.valueOf(item.latitude));

        convertView.findViewById(R.id.clickView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listActivity.onItemClicked(item);
            }
        });

        return convertView;
    }
}
