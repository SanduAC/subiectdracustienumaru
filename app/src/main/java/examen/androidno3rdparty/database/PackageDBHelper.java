package examen.androidno3rdparty.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import examen.androidno3rdparty.models.DataPackage;

public class PackageDBHelper {
    public static List<DataPackage> readFromDB(Context context) {
        List<DataPackage> list = new ArrayList<>();
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " +
                DatabaseContract.PackageTable.TABLE_NAME, null);
        while (cursor.moveToNext()) {
            DataPackage dataPackage = new DataPackage();
            dataPackage.packageId = cursor.getInt(cursor.getColumnIndex(DatabaseContract.PackageTable.COLUMN_NAME_PACKAGE_ID));
            dataPackage.latitude = cursor.getDouble(cursor.getColumnIndex(DatabaseContract.PackageTable.COLUMN_NAME_LATITUDE));
            dataPackage.longitude = cursor.getDouble(cursor.getColumnIndex(DatabaseContract.PackageTable.COLUMN_NAME_LONGITUDE));
            dataPackage.timestamp = cursor.getLong(cursor.getColumnIndex(DatabaseContract.PackageTable.COLUMN_NAME_TIMESTAMP));
            int type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.PackageTable.COLUMN_NAME_TIMESTAMP));
            if (type == 0) {
                dataPackage.packageType = DataPackage.PackageType.position;
            } else {
                dataPackage.packageType = DataPackage.PackageType.state;
            }
            list.add(dataPackage);
        }
        return list;
    }

    public static void saveInDB(Context context, List<DataPackage> dataPackages) {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.execSQL("delete from " + DatabaseContract.PackageTable.TABLE_NAME);
        for (DataPackage dataPackage : dataPackages) {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseContract.PackageTable.COLUMN_NAME_PACKAGE_ID, dataPackage.packageId);
            cv.put(DatabaseContract.PackageTable.COLUMN_NAME_LATITUDE, dataPackage.latitude);
            cv.put(DatabaseContract.PackageTable.COLUMN_NAME_LONGITUDE, dataPackage.longitude);
            cv.put(DatabaseContract.PackageTable.COLUMN_NAME_TIMESTAMP, dataPackage.timestamp);
            int type;
            if (dataPackage.packageType.equals(DataPackage.PackageType.position)) {
                type = 0;
            } else {
                type = 1;
            }
            cv.put(DatabaseContract.PackageTable.COLUMN_NAME_PACKAGE_TYPE, type);
            db.insert(DatabaseContract.PackageTable.TABLE_NAME, null, cv);
        }
    }
}
