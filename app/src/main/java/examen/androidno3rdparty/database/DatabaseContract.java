package examen.androidno3rdparty.database;

import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String DB_NAME = "thisappdatabase.db";
    public static final int DB_VERSION = 1;

    public class PackageTable implements BaseColumns {
        public static final String TABLE_NAME = "packages";

        public static final String COLUMN_NAME_PACKAGE_ID = "package_id";
        public static final String COLUMN_NAME_PACKAGE_TYPE = "package_type";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

        public static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_PACKAGE_ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_PACKAGE_TYPE + " INTEGER, " +
                        COLUMN_NAME_LATITUDE + " DOUBLE, " +
                        COLUMN_NAME_LONGITUDE + " DOUBLE, " +
                        COLUMN_NAME_TIMESTAMP + " FLOAT)";

        public static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }
}
