package examen.androidno3rdparty.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataPackagesForIntent implements Serializable {
    public List<DataPackage> dataPackages;

    public DataPackagesForIntent(List<DataPackage> dataPackages) {
        this.dataPackages = dataPackages;
    }
}
