package examen.androidno3rdparty.tools;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import examen.androidno3rdparty.models.DataPackage;

public class ChartView extends View {
    private List<DataPackage> data;

    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData(List<DataPackage> data) {
        ChartView.this.data = new ArrayList<>();
        ChartView.this.data.addAll(data);
    }


//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//
//
//        int width=canvas.getWidth();
//        int height=canvas.getHeight();
//
//        Paint paint = new Paint();
//        canvas.drawPaint(paint);
//        paint.setColor(Color.BLACK);
//        paint.setTextSize(16);
//        canvas.drawText("10", 10, 10, paint);

//    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width=canvas.getWidth();
        int height=canvas.getHeight();

        int step=(width-160)/24;
        int start=80;
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(40);
        canvas.drawText("01:00", start,height-20,paint);
        start+=step;
        canvas.drawText("02:00", start,height-45,paint);
        start+=step;
        canvas.drawText("03:00", start,height-20,paint);
        start+=step;
        canvas.drawText("04:00", start,height-45,paint);
        start+=step;
        canvas.drawText("05:00", start,height-20,paint);
        start+=step;
        canvas.drawText("06:00", start,height-45,paint);
        start+=step;
        canvas.drawText("07:00", start,height-20,paint);
        start+=step;
        canvas.drawText("08:00", start,height-45,paint);
        start+=step;
        canvas.drawText("09:00", start,height-20,paint);
        start+=step;
        canvas.drawText("10:00", start,height-45,paint);
        start+=step;
        canvas.drawText("11:00", start,height-20,paint);
        start+=step;
        canvas.drawText("12:00", start,height-45,paint);
        start+=step;
        canvas.drawText("13:00", start,height-20,paint);
        start+=step;
        canvas.drawText("14:00", start,height-45,paint);
        start+=step;
        canvas.drawText("15:00", start,height-20,paint);
        start+=step;
        canvas.drawText("16:00", start,height-45,paint);
        start+=step;
        canvas.drawText("17:00", start,height-20,paint);
        start+=step;
        canvas.drawText("18:00", start,height-45,paint);
        start+=step;
        canvas.drawText("19:00", start,height-20,paint);
        start+=step;
        canvas.drawText("20:00", start,height-45,paint);
        start+=step;
        canvas.drawText("21:00", start,height-20,paint);
        start+=step;
        canvas.drawText("22:00", start,height-45,paint);
        start+=step;
        canvas.drawText("23:00", start,height-20,paint);
        start+=step;
        canvas.drawText("24:00", start,height-45,paint);

        Paint linePaint=new Paint();
        linePaint.setColor(Color.BLACK);
        linePaint.setStrokeWidth(10);
        canvas.drawLine(70, height-80, width-40, height-80, linePaint);
        canvas.drawLine(70, height-80, 70, 70, linePaint);

    }
}
